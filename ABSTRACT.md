# README #
Technological innovations concerning instruments for the management of legal legal processes have reached the 
Federal courts however in the State Administrative courts its adoption remains low and slow due to the measures of 
the plans of the resources allocated. Nevertheless, it is recognized that the use of specialized computer tools offers 
citizens transparency in the processes and a more prompt and expeditious justice. In this research, a low-cost 
information system was designed to monitor the demands made by citizens. It was implemented in the Administrative 
Litigation Tribunal (TCA) to conduct online trials in the state of Colima. To manage the information system lifecycle, 
the Agile Unified Process (AUP) methodology was used, allowing the priority requirements to be addressed in short 
time frames and complying in a timely manner to implement them; In addition because it allows the independence in 
the use of programming technologies situation that propitió that technologies will be used of open code and will 
impact in a low cost. The implanted system offers an innovative approach that allows to take the online trial, 
favoring the citizens in the administrative legal processes before an instance of some institution of government; 
Using methods of verification points to clarify the evolution of the demand in progress, verdict, compliance with 
the judgment and in each of the phases of attention: Parties' Office, Secretary of Agreements, Actuaries, 
Judgment Projects and Judge of the Court of Administrative Dispute in the state of Colima.